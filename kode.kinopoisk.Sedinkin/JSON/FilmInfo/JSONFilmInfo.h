//
//  JSONFilmInfo.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface JSONFilmInfo : JSONModel

@property (strong, nonatomic) NSString<Optional> *filmID;
@property (strong, nonatomic) NSString<Optional> *nameRU;
@property (strong, nonatomic) NSString<Optional> *genre;
@property (strong, nonatomic) NSString<Optional> *slogan;
@property (strong, nonatomic) NSString<Optional> *filmDescription;
@property (strong, nonatomic) NSString<Optional> *premier;

@end
