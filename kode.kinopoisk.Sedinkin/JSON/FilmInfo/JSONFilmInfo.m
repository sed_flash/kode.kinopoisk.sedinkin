//
//  JSONFilmInfo.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "JSONFilmInfo.h"

@implementation JSONFilmInfo

+ (JSONKeyMapper *)keyMapper {
    NSDictionary *map = @{@"filmDescription" : @"description",
                          @"premier"         : @"rentData.premiereRU"};
    
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:map];
}

@end
