//
//  AdditionalCityInfo.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface AdditionalCityInfo : JSONModel

@property (nonatomic, assign) int cityID;
@property (nonatomic, strong) NSString *cityName;

@end
