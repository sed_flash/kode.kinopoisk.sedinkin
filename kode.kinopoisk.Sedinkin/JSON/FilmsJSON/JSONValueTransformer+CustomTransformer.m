//
//  JSONValueTransformer+CustomTransformer.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "JSONValueTransformer+CustomTransformer.h"

@implementation JSONValueTransformer (CustomTransformer)

- (UIImage *)UIImageFromNSString:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    return image;
}

- (NSData *)JSONObjectFromUIImage:(UIImage *)image {
    return nil; // transformed object
}

@end
