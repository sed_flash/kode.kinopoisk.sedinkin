//
//  FilmsModel.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol AdditionalFilmsInfo
@end

@interface FilmsList : JSONModel

@property (strong, nonatomic) NSArray<AdditionalFilmsInfo> *filmsData;

@end
