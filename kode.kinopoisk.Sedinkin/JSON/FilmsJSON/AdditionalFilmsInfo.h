//
//  AdditionalFilmsInfo.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>

@interface AdditionalFilmsInfo : JSONModel

@property (assign, nonatomic) int id;
@property (strong, nonatomic) NSString *nameRU;
@property (assign, nonatomic) float rating;
//@property (strong, nonatomic) UIImage *posterURL;
@property (strong, nonatomic) NSString<Optional> *genre;

@end
