//
//  JSONValueTransformer+CustomTransformer.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>

@interface JSONValueTransformer (CustomTransformer)

- (UIImage *)UIImageFromNSString:(NSString *)imageName;

- (NSString *)JSONObjectFromUIImage:(UIImage *)image;


@end
