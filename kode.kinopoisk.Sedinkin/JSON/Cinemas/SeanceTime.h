//
//  SeanceTime.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface SeanceTime : JSONModel

@property (strong, nonatomic) NSString *time;

@end
