//
//  SeanceInfo.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SeanceTime.h"

@protocol SeanceTime
@end

@interface SeanceJSONInfo : JSONModel

@property (strong, nonatomic) NSString<Optional> *address;
@property (strong, nonatomic) NSString<Optional> *lon;
@property (strong, nonatomic) NSString<Optional> *lat;
@property (strong, nonatomic) NSString<Optional> *cinemaName;
@property (strong, nonatomic) NSArray<SeanceTime, Optional> *seance;

@end
