//
//  CinemasJSON.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SeanceJSONInfo.h"

@protocol SeanceJSONInfo
@end

@interface CinemasJSON : JSONModel


//@property (strong, nonatomic) NSString *cityID;
//@property (strong, nonatomic) NSString *cityName;
//@property (strong, nonatomic) NSString *seanceURL;
//@property (strong, nonatomic) NSString *filmID;
//@property (strong, nonatomic) NSString *nameRU;
//@property (strong, nonatomic) NSString *nameEN;
//@property (strong, nonatomic) NSString *year;
//@property (strong, nonatomic) NSString *rating;
//@property (strong, nonatomic) NSString *posterURL;
//@property (strong, nonatomic) NSString *filmLength;
//@property (strong, nonatomic) NSString *country;
//@property (strong, nonatomic) NSString *genre;
@property (strong, nonatomic) NSString<Optional> *date;
@property (strong, nonatomic) NSArray<SeanceJSONInfo, Optional> *items;

@end


//{"cityID":"1","cityName":"\u041c\u043e\u0441\u043a\u0432\u0430","seanceURL":"http:\/\/www.kinopoisk.ru\/level\/8\/film\/910327\/tc\/1\/","filmID":"910327","nameRU":"\u041d\u0435 \u0434\u044b\u0448\u0438","nameEN":"Don't Breathe","year":"2016","rating":"6.9 (8 176)","posterURL":"film_iphone\/iphone_910327.jpg","filmLength":"1:28","country":"\u0421\u0428\u0410","genre":"\u0443\u0436\u0430\u0441\u044b, \u0442\u0440\u0438\u043b\u043b\u0435\u0440, \u043a\u0440\u0438\u043c\u0438\u043d\u0430\u043b","videoURL":{"hd":"http:\/\/kp.cdn.yandex.net\/trailers\/910327\/kinopoisk.ru-Don_t-Breathe-308151.mp4","sd":"http:\/\/kp.cdn.yandex.net\/trailers\/910327\/kinopoisk.ru-Don_t-Breathe-308150.mp4","low":"http:\/\/kp.cdn.yandex.net\/trailers\/910327\/kinopoisk.ru-Don_t-Breathe-308159.mp4"},
//    "items":[
//    {"cinemaID":263287,"address":"\u0414\u0440\u0443\u0436\u0438\u043d\u043d\u0438\u043a\u043e\u0432\u0441\u043a\u0430\u044f, 15","metro":"\u043c. \u041a\u0440\u0430\u0441\u043d\u043e\u043f\u0440\u0435\u0441\u043d\u0435\u043d\u0441\u043a\u0430\u044f","lon":"37.5746980","lat":"55.7597550","cinemaName":"\u041a\u0438\u043d\u043e\u0446\u0435\u043d\u0442\u0440 \u00ab\u0421\u043e\u043b\u043e\u0432\u0435\u0439\u00bb \u043d\u0430 \u041a\u0440\u0430\u0441\u043d\u043e\u0439 \u041f\u0440\u0435\u0441\u043d\u0435",
//        "seance":[
//        {"time":"14:25"},
//        {"time":"20:35"}]
//    },
//    {"cinemaID":264559,"address":"\u043f\u0440\u043e\u0441\u043f. \u041c\u0438\u0440\u0430, 211, \u00ab\u0417\u043e\u043b\u043e\u0442\u043e\u0439 \u0412\u0430\u0432\u0438\u043b\u043e\u043d\u00bb","metro":"\u043c. \u0421\u0432\u0438\u0431\u043b\u043e\u0432\u043e, \u043c. \u0411\u043e\u0442\u0430\u043d\u0438\u0447\u0435\u0441\u043a\u0438\u0439 \u0441\u0430\u0434","lon":"37.662739","lat":"55.844052","cinemaName":"\u041b\u044e\u043a\u0441\u043e\u0440 \u0426\u0435\u043d\u0442\u0440",
//        "seance":[
//        {"time":"23:20"}]
//    }],
//    "date":"01.10.2016","imdbID":"tt4160708"}
