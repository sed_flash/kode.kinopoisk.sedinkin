//
//  CitiesList.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "AdditionalCityInfo.h"

@protocol AdditionalCityInfo
@end

@interface CitiesList : JSONModel

@property (nonatomic, assign) int countryID;
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSArray<AdditionalCityInfo> *cityData;

@end


