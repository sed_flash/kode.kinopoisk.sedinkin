//
//  ViewController.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "ViewController.h"
#import "CitiesTableView.h"
#import "CitiesListModel.h"
#import "FilmInfoViewController.h"
#import "ListOfFilmsViewController.h"

static NSInteger const TAG_CITIES_VIEW = 53;
static NSInteger const CITIES_VIEW_INCLINE_ANGLE = 90;
static NSString *const APP_NAME = @"kode.kinopoisk.Sedinkin";

@interface ViewController () <UITextFieldDelegate, CityTableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (strong, nonatomic) CitiesListModel *citiesModel;
@property (strong, nonatomic) CitiesTableView *citiesTableView;
@property (assign, nonatomic) float currentKeyboardHeight;
@end

@implementation ViewController

static inline float radians(float angle) {
    return angle * M_PI / 180;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = APP_NAME;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:17]}];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(installTextFieldDelegate)
                                                 name:listOfCitiesReceived
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _citiesModel = [[CitiesListModel alloc] init];
}

- (void)prepareToCloseTableView {
    [self hideCitiesViewWithAnimation];
}

- (void)finallyCloseTableView {
    [_cityField resignFirstResponder];
    _citiesTableView.delegate = nil;
    _citiesTableView = nil;
    for (UIView *v in [self.view subviews]) {
        if (v.tag == TAG_CITIES_VIEW) {
            [v removeFromSuperview];
        }
    }
}

#pragma mark - Notifications

- (void)installTextFieldDelegate {
    _cityField.delegate = self;
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _currentKeyboardHeight = kbSize.height;
    
    float height = CGRectGetHeight(self.view.frame) - _currentKeyboardHeight - CGRectGetMaxY(_cityField.frame);
    CGRect citiesTableViewRect = CGRectMake(_cityField.frame.origin.x,
                                            CGRectGetMaxY(_cityField.frame) - height / 2,
                                            CGRectGetWidth(_cityField.frame),
                                            height);
    
    _citiesTableView = [[CitiesTableView alloc] initWithFrame:citiesTableViewRect];
    _citiesTableView.delegate = self;
    [_citiesTableView setTag:TAG_CITIES_VIEW];
    
    _citiesTableView.alpha = 0.3;
    [self.view addSubview:_citiesTableView];
    
// Анимация таблицы со списком городов
// "выплывает" из под text field
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = -1.0 / 1000;
    
    transform = CATransform3DRotate(transform, radians(CITIES_VIEW_INCLINE_ANGLE), 1.0, 0.0, 0.0);
    _citiesTableView.layer.transform = transform;
    
    CGRect endedRect = citiesTableViewRect;
    endedRect.origin.y = CGRectGetMaxY(_cityField.frame);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.3 animations:^{
            _citiesTableView.frame = endedRect;
            _citiesTableView.alpha = 1.0;
            _citiesTableView.layer.transform = CATransform3DIdentity;
        }];
        
    });
}


- (void)keyboardWillHide:(NSNotification*)notification {
    [self hideCitiesViewWithAnimation];
}

- (void)hideCitiesViewWithAnimation {
    CATransform3D transform = _citiesTableView.layer.transform;
    transform.m34 = -1.0 / 1000;
    
    transform = CATransform3DRotate(transform, radians(CITIES_VIEW_INCLINE_ANGLE), 1.0, 0.0, 0.0);
    
    CGRect endedCitiesViewRect = _citiesTableView.frame;
    endedCitiesViewRect.origin.y = CGRectGetMaxY(_cityField.frame) - CGRectGetHeight(_citiesTableView.frame) / 2;
    [UIView setAnimationsEnabled:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.3 animations:^{
            _citiesTableView.frame = endedCitiesViewRect;
            _citiesTableView.alpha = 0.3;
            _citiesTableView.layer.transform = transform;
        } completion:^(BOOL finished) {
            if (finished) {
                [self finallyCloseTableView];
            }
        }];
        
    });

}

#pragma mark - UITextField delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self checkCorrectionCityName:textField.text];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

#pragma mark - CityTableViewDelegate

- (void)setChoosingCity:(NSString *)city cityID:(int)cityID{
    if (city) {
        _cityField.text = city;
        [HTTPClient sharedInstance].cityID = cityID;
    }
}
- (void)closeTableView {
    [self prepareToCloseTableView];
}

- (NSUInteger)citiesCount:(CitiesTableView *)citiesTableView {
    return [_citiesModel citiesCount];
}

- (AdditionalCityInfo *)cityForIndex:(CitiesTableView *)citiesTableView index:(NSInteger)index {
    return [_citiesModel cityForIndex:index];
}

#pragma mark - Managment

// Метод сравнивает введеный в text field текст со списком городов
// полученным от kinopoisk
- (void)checkCorrectionCityName:(NSString *)cityName {
    if (cityName) {
        if ([_citiesModel correctCityName:cityName]) {
            [self showListOfFilms];
        } else {
            _cityField.text = @"";
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List of Films

- (void)showListOfFilms {
    ListOfFilmsViewController *listOfFilmsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ListOfFilmsVCStoryboard"];
    [self.navigationController pushViewController:listOfFilmsVC animated:YES];
}


@end
