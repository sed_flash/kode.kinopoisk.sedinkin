//
//  FilmInfoViewController.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "FilmInfoViewController.h"
#import "FilmInfoModel.h"
#import "JSONFilmInfo.h"
#import "SeanceTableViewController.h"

@interface FilmInfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *filmNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *filmDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *filmGenreLabel;
@property (weak, nonatomic) IBOutlet UILabel *filmDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *filmSliganLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) FilmInfoModel *filmInfo;
@property (strong, nonatomic) JSONFilmInfo *jsonFilmInfo;

@end

@implementation FilmInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(infoAboutFilm)
                                                 name:infoAboutFilmReceived
                                               object:nil];
    
    [self clearLabels];
    
// Получение информвции о фильме от kinopoisk
    _filmInfo = [[FilmInfoModel alloc] initWithFilmId:_choosingFilmId];
    [self startActivityIndicator];
}

- (void)startActivityIndicator {
    [_activityIndicator startAnimating];
    [_activityIndicator setHidesWhenStopped:YES];
    [_mainView setHidden:YES];
}

- (void)stopActivityIndicator {
    [_activityIndicator stopAnimating];
    [_mainView setHidden:NO];
}

#pragma amrk - Notifications

- (void)infoAboutFilm {
    [self stopActivityIndicator];
    _jsonFilmInfo = [_filmInfo infoAboutFilm];
    
    [_filmNameLabel setText:_jsonFilmInfo.nameRU];
    [_filmDateLabel setText:_jsonFilmInfo.premier];
    [_filmGenreLabel setText:_jsonFilmInfo.genre];
    [_filmDescriptionLabel setText:_jsonFilmInfo.filmDescription];
    [_filmSliganLabel setText:_jsonFilmInfo.slogan];
}

#pragma mark - Managment 

- (void)clearLabels {
    [_filmNameLabel setText:@""];
    [_filmDateLabel setText:@""];
    [_filmGenreLabel setText:@""];
    [_filmDescriptionLabel setText:@""];
    [_filmSliganLabel setText:@""];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"GetSeance"]) {
        SeanceTableViewController *seanceVC = [segue destinationViewController];
        seanceVC.filmId = _jsonFilmInfo.filmID;
        seanceVC.filmName = _jsonFilmInfo.nameRU;
    }
}


@end
