//
//  SeanceTableViewController.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeanceTableViewController : UITableViewController

@property (strong, nonatomic) NSString *filmId;
@property (strong, nonatomic) NSString *filmName;

@end
