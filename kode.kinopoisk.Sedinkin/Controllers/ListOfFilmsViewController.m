//
//  ListOfFilmsViewController.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "ListOfFilmsViewController.h"
#import "FilmTableViewCell.h"
#import "AdditionalFilmsInfo.h"
#import "FilmsModel.h"
#import "FilmInfoViewController.h"

#define FILMS_CELL @"filmsCell"

static NSString *const SORT_BY_GENRE  = @"genre";
static NSString *const SORT_BY_RATING = @"rating";

@interface ListOfFilmsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) FilmsModel *filmsModel;
@property (weak, nonatomic) IBOutlet UISwitch *ratingSort;
@property (weak, nonatomic) IBOutlet UISwitch *ganreSort;
@property (weak, nonatomic) IBOutlet UITableView *filmsTableView;

@end

@implementation ListOfFilmsViewController {
    NSArray *sortedByGenre;
    NSArray *sortedByRating;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Прокат";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:17]}];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(listOfFilmsReceived)
                                                 name:listOfFilmsReceived
                                               object:nil];
// Получение списка фильмов от kinopoisk
    _filmsModel = [[FilmsModel alloc] init];
    
// Массивы для сортировки по жанру и рейтингу соответственно
    sortedByGenre = [NSArray new];
    sortedByRating = [NSArray new];
    
    _filmsTableView.dataSource = self;
    _filmsTableView.delegate = self;
    [_filmsTableView registerNib:[UINib nibWithNibName:@"FilmCellXIB" bundle:nil] forCellReuseIdentifier:FILMS_CELL];
    _filmsTableView.rowHeight = UITableViewAutomaticDimension;
    _filmsTableView.estimatedRowHeight = 73.0;
    
    [_ratingSort setOn:NO];
    [_ganreSort setOn:NO];
}

#pragma mark - Notification

- (void)listOfFilmsReceived {
    [_filmsTableView reloadData];
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_filmsModel filmsCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FilmTableViewCell *filmCell = [tableView dequeueReusableCellWithIdentifier:FILMS_CELL];
    
    AdditionalFilmsInfo *film;
// Выборка данных для отображения производится в зависимости от статуса переключателей
    if (_ratingSort.isOn) {
        film = [sortedByRating objectAtIndex:indexPath.row];
    } else if (_ganreSort.isOn) {
        film = [sortedByGenre objectAtIndex:indexPath.row];
    } else {
        film = [_filmsModel filmForIndex:indexPath.row];
    }
    
    filmCell.film = film.nameRU;
    filmCell.genre = film.genre;
    filmCell.filmRating = film.rating;
    
    return filmCell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int filmId;
// Для корректной работы происходит проверка какой из переключателей активен
    if (_ratingSort.isOn) {
        AdditionalFilmsInfo *sortedFilmInfo = [sortedByRating objectAtIndex:indexPath.row];
        filmId = sortedFilmInfo.id;
    } else if (_ganreSort.isOn) {
        AdditionalFilmsInfo *sortedFilmInfo =  [sortedByGenre objectAtIndex:indexPath.row];
        filmId = sortedFilmInfo.id;
    } else {
        filmId = [_filmsModel filmForIndex:indexPath.row].id;
    }
    
    FilmInfoViewController *filmInfoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FilmInfoViewControllerStoryboard"];
    filmInfoVC.choosingFilmId = [NSString stringWithFormat:@"%d", filmId];;
    
    [self.navigationController pushViewController:filmInfoVC animated:YES];
    
}

#pragma mark - Actions

- (IBAction)sortAtRating:(id)sender {
    if (_ganreSort.isOn) {
        [_ganreSort setOn:NO];
    }
    
    sortedByRating = [self sortArray:[[_filmsModel arrayOfFilms].filmsData copy]
                                  by:SORT_BY_RATING
                           ascending:NO];
    
    [_filmsTableView reloadData];
}

- (IBAction)sortAtGanre:(id)sender {
    if (_ratingSort.isOn) {
        [_ratingSort setOn:NO];
    }
    
    sortedByGenre = [self sortArray:[[_filmsModel arrayOfFilms].filmsData copy]
                                 by:SORT_BY_GENRE
                          ascending:YES];
    
    [_filmsTableView reloadData];
}

// Метод сортирует массив по указанному ключу по возрастанию/убыванию
- (NSArray *)sortArray:(NSArray *)sortingArray by:(id)sortedBy ascending:(BOOL)ascending {
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:sortedBy ascending:ascending];
    return [sortingArray sortedArrayUsingDescriptors:@[descriptor]];
}

#pragma mark - Management

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
