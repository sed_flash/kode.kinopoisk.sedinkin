//
//  SeanceTableViewController.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "SeanceTableViewController.h"
#import "CinemaAndSeanceModel.h"
#import "CinemaTableViewCell.h"
#import "SeanceJSONInfo.h"
#import "SeanceTime.h"
#import "CinemaMap.h"
#import "CinemaInfoView.h"

#define CINEMA_CELL @"cinemaCell"
#define NAVIGATION_BAR_HEIGH self.navigationController.navigationBar.frame.size.height
static NSInteger const TAG_CINEMA_MAP_VIEW = 54;
static NSInteger const TAG_CINEMA_INFO_VIEW = 55;

@interface SeanceTableViewController () <CinemaMapViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CinemaAndSeanceModel *seanceModel;
@property (strong, nonatomic) CinemaMap *cinemaMap;
@property (strong, nonatomic) CinemaInfoView *cinemaInfoView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation SeanceTableViewController {
    BOOL cinemaInfoViewIsDisplay;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
// Получение списка кинотеатров, в котороых идет фильм
    _seanceModel = [[CinemaAndSeanceModel alloc] initWithFilmId:_filmId];
    
    [self configuratingNavigationBar];
    [self installObservers];
    [self.tableView registerNib:[UINib nibWithNibName:@"CinemaTableViewCell" bundle:nil] forCellReuseIdentifier:CINEMA_CELL];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 73.0;
    [self startActivityIndicator];
}

- (void)configuratingNavigationBar {
    self.title = _filmName;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:17]}];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
}

- (void)installObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(seanceReceived)
                                                 name:cinemasReceived
                                               object:nil];
}

- (void)startActivityIndicator {
    [_activityIndicator startAnimating];
    [_activityIndicator setHidesWhenStopped:YES];
}

- (void)stopActivityIndicator {
    [_activityIndicator stopAnimating];
}

#pragma mark - Notification

- (void)seanceReceived {
    [self stopActivityIndicator];
    if ([_seanceModel cinemasCount] == 0) {
        [[HTTPClient sharedInstance] showAlertView:self withTitle:@"Пусто :(" message:@"Нет ни одного кинотеатра."];
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_seanceModel cinemasCount];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CinemaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CINEMA_CELL forIndexPath:indexPath];
    
    SeanceJSONInfo *seanceInfo = [_seanceModel cinemaAtIndex:indexPath.row];
    cell.cName = seanceInfo.cinemaName;
    cell.cAddress = seanceInfo.address;
    cell.sTime = [self addSeances:seanceInfo];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self showMapWithCinema:[_seanceModel cinemaAtIndex:indexPath.row]];
}

#pragma mark - Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// Метод принимает информацию о сеансе и проверяет есть ли в списке
// расписание сенсов.
// Возвращает пустую строку, если расписание сеансов отсутствует

- (NSString *)addSeances:(SeanceJSONInfo *)seanceInfo {
    SeanceTime *seanceTime;
    
    // Если есть больше 1 санса
    if (seanceInfo.seance.count > 1) {
        NSMutableString *mutableSeance = [NSMutableString new];
        for (SeanceTime *seance in seanceInfo.seance) {
            seanceTime = seance;
            [mutableSeance appendString:[NSString stringWithFormat:@"%@\n", seance.time]];
        }
        return mutableSeance;
    } else if (seanceInfo.seance.count == 1) { // Всего один сеанс в кинотеатре
        seanceTime = [seanceInfo.seance objectAtIndex:0];
        return seanceTime.time;
    }
    
    return @"";
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGRect frame = self.cinemaMap.frame;
    if (cinemaInfoViewIsDisplay) {
        frame.origin.y = scrollView.contentOffset.y + self.cinemaMap.frame.size.height / 2 - CGRectGetHeight(_cinemaInfoView.frame) / 2 - NAVIGATION_BAR_HEIGH;
    } else {
        frame.origin.y = scrollView.contentOffset.y + self.cinemaMap.frame.size.height / 2 - NAVIGATION_BAR_HEIGH;
    }
    
    self.cinemaMap.frame = frame;
    
    CGRect cinemaInfoRect = self.cinemaInfoView.frame;
    cinemaInfoRect.origin.y = CGRectGetMaxY(self.cinemaMap.frame) + 5;
    self.cinemaInfoView.frame = cinemaInfoRect;
}

- (BOOL)mapIsDisplayed {
    for (UIView *v in [self.view subviews]) {
        if (v.tag == TAG_CINEMA_MAP_VIEW) {
            return YES;;
        }
    }
    
    return NO;
}

- (BOOL)mapInfoIsDisplayed {
    for (UIView *v in [self.view subviews]) {
        if (v.tag == TAG_CINEMA_INFO_VIEW) {
            return YES;;
        }
    }
    
    return NO;
}

#pragma mark - Cinema Map

- (void)closeCinemaMapView {
    
    CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    CGAffineTransform cinemaInfoTransform = CGAffineTransformIdentity;
    cinemaInfoTransform = CGAffineTransformTranslate(cinemaInfoTransform, 0, -(_cinemaInfoView.frame.size.height + _cinemaMap.frame.size.height / 2));
    cinemaInfoTransform = CGAffineTransformScale(cinemaInfoTransform, 0.1, 0.1);
    
    [UIView animateWithDuration:0.3 animations:^{
        _cinemaMap.transform = transform;
        _cinemaMap.alpha = 0.0;
        _cinemaInfoView.transform = cinemaInfoTransform;
        _cinemaInfoView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self closeCinemaMapViewImmediately];
    }];
}

- (void)closeCinemaMapViewImmediately {
    cinemaInfoViewIsDisplay = NO;
    _cinemaMap.cinemaMapDelegate = nil;
    _cinemaMap = nil;
    for (UIView *v in [self.view subviews]) {
        if (v.tag == TAG_CINEMA_MAP_VIEW || v.tag == TAG_CINEMA_INFO_VIEW) {
            [v removeFromSuperview];
        }
    }
}

- (void)needToShowCinemaInfo:(NSString *)name address:(NSString *)address {
    if (![self mapInfoIsDisplayed]) {
        
        cinemaInfoViewIsDisplay = YES;
        
        CGRect cinemaInfoRect = CGRectMake(_cinemaMap.frame.origin.x,
                                           CGRectGetMaxY(_cinemaMap.frame) - 25.5,
                                           CGRectGetWidth(_cinemaMap.frame),
                                           61);
        _cinemaInfoView = [[NSBundle mainBundle] loadNibNamed:@"CinemaInfo" owner:self options:nil][0];
        [_cinemaInfoView setFrame:cinemaInfoRect];
        [_cinemaInfoView setTag:TAG_CINEMA_INFO_VIEW];
        _cinemaInfoView.cinema = name;
        _cinemaInfoView.address = address;
        
        [self.view insertSubview:_cinemaInfoView belowSubview:_cinemaMap];
        
        _cinemaInfoView.transform = CGAffineTransformTranslate(_cinemaInfoView.transform, 0, -_cinemaInfoView.frame.size.height);
        _cinemaInfoView.transform = CGAffineTransformScale(_cinemaInfoView.transform, 0.3, 0.3);
        _cinemaInfoView.alpha = 0.3;
        
        CGRect newRectForCinemaMap = _cinemaMap.frame;
        
        newRectForCinemaMap.origin.y -= 30.5;
        
        [UIView animateWithDuration:0.3 animations:^{
            _cinemaInfoView.transform = CGAffineTransformIdentity;
            _cinemaInfoView.alpha = 1.0;
            [_cinemaMap setFrame:newRectForCinemaMap];
        } completion:^(BOOL finished) {
            [self.view bringSubviewToFront:_cinemaInfoView];
        }];
    }
}

- (void)showMapWithCinema:(SeanceJSONInfo *)cinemaInfo {
    if (![self mapIsDisplayed]) {
        [self closeCinemaMapViewImmediately];
        
        CGRect mapRect = CGRectMake(WidthWithPercent(5),
                                    HeightWithPercent(25) + self.tableView.contentOffset.y - NAVIGATION_BAR_HEIGH,
                                    WidthWithPercent(90),
                                    HeightWithPercent(50));
        
        _cinemaMap = [[NSBundle mainBundle] loadNibNamed:@"CinemaMap" owner:self options:nil][0];
        [_cinemaMap setFrame:mapRect];
        [_cinemaMap setTag:TAG_CINEMA_MAP_VIEW];
        _cinemaMap.cinemaMapDelegate = self;
        [_cinemaMap setCameraToCinema:cinemaInfo];
        
        [self.view addSubview:_cinemaMap];
        
        CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
        _cinemaMap.transform = transform;
        _cinemaMap.alpha = 0.2;
        
        [UIView animateWithDuration:0.3 animations:^{
            _cinemaMap.transform = CGAffineTransformIdentity;
            _cinemaMap.alpha = 1.0;
        }];
    }
    
}

- (IBAction)getHelp:(id)sender {
    [[HTTPClient sharedInstance] showAlertView:self withTitle:@"Подсказка!" message:@"Чтобы увидеть кинотеатр на карте просто:\n1) кликните на понравившейся;\n2) кликните по маркеру на отобразившейся карте для дополнительной информации."];
}


@end
