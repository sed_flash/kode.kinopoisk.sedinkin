//
//  Constants.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const listOfCitiesReceived;
FOUNDATION_EXPORT NSString *const infoAboutFilmReceived;
FOUNDATION_EXPORT NSString *const cinemasReceived;
FOUNDATION_EXPORT NSString *const listOfFilmsReceived;
