//
//  HTTPClient.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "HTTPClient.h"

@implementation HTTPClient

+ (HTTPClient *)sharedInstance {
    static HTTPClient *instance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        instance = [[HTTPClient alloc] init];
    });
    
    return instance;
}

- (void)httpGETQueryToURL:(NSURL *)url success:(void (^)(id response))success failure:(void (^)(NSString *error))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(@"Internet connection problem");
    }];
}

- (void)showAlertView:(NSObject *)object withTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:object cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    [alertView show];
}

@end
