//
//  Stylization.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "Stylization.h"

@implementation Stylization

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    
    return instance;
}

- (void)drawGradientInView:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)startColor.CGColor, (id)endColor.CGColor, nil];
    [view.layer insertSublayer:gradient atIndex:0];
}

@end
