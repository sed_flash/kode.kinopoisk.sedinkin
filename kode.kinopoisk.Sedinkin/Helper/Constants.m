//
//  Constants.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "Constants.h"

NSString *const listOfCitiesReceived  = @"listOfCitiesReceived";
NSString *const infoAboutFilmReceived = @"infoAboutFilmReceived";
NSString *const cinemasReceived = @"cinemasReceived";
NSString *const listOfFilmsReceived = @"listOfFilmsReceived";
