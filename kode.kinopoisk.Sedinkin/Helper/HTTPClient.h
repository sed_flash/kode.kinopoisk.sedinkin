//
//  HTTPClient.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface HTTPClient : NSObject

+ (HTTPClient *)sharedInstance;
- (void)httpGETQueryToURL:(NSURL *)url success:(void (^)(id response))success failure:(void (^)(NSString *error))failure;
- (void)showAlertView:(NSObject *)object withTitle:(NSString *)title message:(NSString *)message;

@property (assign, nonatomic) int cityID;

@end
