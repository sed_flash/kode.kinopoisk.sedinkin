//
//  Stylization.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Stylization : NSObject

+ (instancetype)sharedInstance;

- (void)drawGradientInView:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor;

@end
