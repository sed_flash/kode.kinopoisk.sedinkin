//
//  CitiesListModel.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdditionalCityInfo.h"

@interface CitiesListModel : NSObject

- (NSUInteger)citiesCount;
- (AdditionalCityInfo *)cityForIndex:(NSInteger)index;
- (BOOL)correctCityName:(NSString *)cityName;

@end
