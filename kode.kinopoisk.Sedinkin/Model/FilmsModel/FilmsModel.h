//
//  FilmsModel.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmsList.h"

@class AdditionalFilmsInfo;
@interface FilmsModel : NSObject

- (NSUInteger)filmsCount;
- (AdditionalFilmsInfo *)filmForIndex:(NSInteger)index;
- (FilmsList *)arrayOfFilms;

@end
