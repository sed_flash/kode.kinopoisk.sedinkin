//
//  FilmsModel.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "FilmsModel.h"

static NSString *const filmListURL = @"http://api.kinopoisk.cf/getTodayFilms?cityID=490";

@interface FilmsModel () <UIAlertViewDelegate>

@property (nonatomic, strong) FilmsList *filmsList;

@end

@implementation FilmsModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self getFilms];
    }
    
    return self;
}

- (void)getFilms {
    
    NSURL *url = [NSURL URLWithString:filmListURL];
    [[HTTPClient sharedInstance] httpGETQueryToURL:url success:^(id response) {
        self.filmsList = [[FilmsList alloc] initWithDictionary:response error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:listOfFilmsReceived object:nil];
    } failure:^(NSString *error) {
        [[HTTPClient sharedInstance] showAlertView:self withTitle:@"Error" message:error];
    }];
    
}

- (NSUInteger)filmsCount {
    return _filmsList.filmsData.count;
}

- (AdditionalFilmsInfo *)filmForIndex:(NSInteger)index {
    AdditionalFilmsInfo *film = [_filmsList.filmsData objectAtIndex:index];
    return film;
}

- (AdditionalFilmsInfo *)arrayOfFilms {
    return _filmsList;
}

@end
