//
//  CitiesListModel.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "CitiesListModel.h"
#import "CitiesList.h"

static NSString *const cityListURL = @"http://api.kinopoisk.cf/getCityList?countryID=2";

@interface CitiesListModel () <UIAlertViewDelegate>

@property (nonatomic, strong) CitiesList *citiesList;

@end


@implementation CitiesListModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self getCities];
    }
    
    return self;
}

- (void)getCities {
    
    NSURL *url = [NSURL URLWithString:cityListURL];
    [[HTTPClient sharedInstance] httpGETQueryToURL:url success:^(id response) {
        self.citiesList = [[CitiesList alloc] initWithDictionary:response error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:listOfCitiesReceived object:nil];
    } failure:^(NSString *error) {
        [[HTTPClient sharedInstance] showAlertView:self withTitle:@"Error" message:error];
    }];

}

- (NSUInteger)citiesCount {
    return _citiesList.cityData.count;
}

- (AdditionalCityInfo *)cityForIndex:(NSInteger)index {
    AdditionalCityInfo *cityInfo = [_citiesList.cityData objectAtIndex:index];
    
    return cityInfo;
}

- (BOOL)correctCityName:(NSString *)cityName {
    
    for (AdditionalCityInfo *cityInfo in _citiesList.cityData) {
        if ([[cityInfo.cityName lowercaseString] isEqualToString:[cityName lowercaseString]]) {
            return YES;
        }
    }
    
    return NO;
}

@end
