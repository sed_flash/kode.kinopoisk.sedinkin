//
//  FilmInfoModel.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "FilmInfoModel.h"

@interface FilmInfoModel () <UIAlertViewDelegate>

@property (strong, nonatomic) JSONFilmInfo *jsonFilmInfo;

@end

@implementation FilmInfoModel

inline static NSURL* filmInfoUrlWithId(NSString *filmID) {
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://api.kinopoisk.cf/getFilm?filmID=%@", filmID]];
}

- (id)initWithFilmId:(NSString *)filmId {
    self = [super init];
    if (self) {
        [self getFilmInfo:filmInfoUrlWithId(filmId)];
    }
    
    return self;
}

- (void)getFilmInfo:(NSURL *)filmURL {
    [[HTTPClient sharedInstance] httpGETQueryToURL:filmURL success:^(id response) {
        self.jsonFilmInfo = [[JSONFilmInfo alloc] initWithDictionary:response error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:infoAboutFilmReceived object:nil];
    } failure:^(NSString *error) {
        [[HTTPClient sharedInstance] showAlertView:self withTitle:@"Error" message:error];
    }];
}

- (JSONFilmInfo *)infoAboutFilm {
    return _jsonFilmInfo;
}

@end
