//
//  CinemaAndSeanceModel.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "CinemaAndSeanceModel.h"
#import "CinemasJSON.h"

@interface CinemaAndSeanceModel () <UIAlertViewDelegate>

@property (strong, nonatomic) CinemasJSON *cinemasJSON;

@end

@implementation CinemaAndSeanceModel

inline static NSURL* filmInfoUrlWithFilmID(NSString *filmID) {
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://api.kinopoisk.cf/getSeance?filmID=%@&cityID=490", filmID]];
}

- (id)initWithFilmId:(NSString *)filmId {
    self = [super init];
    if (self) {
        [self getSeanceInfo:filmInfoUrlWithFilmID(filmId)];
    }
    
    return self;
}

- (void)getSeanceInfo:(NSURL *)seance {
    
    [[HTTPClient sharedInstance] httpGETQueryToURL:seance success:^(id response) {
        self.cinemasJSON = [[CinemasJSON alloc] initWithDictionary:response error:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:cinemasReceived object:nil];
    } failure:^(NSString *error) {
        [[HTTPClient sharedInstance] showAlertView:self withTitle:@"Error" message:error];
    }];

}

- (NSUInteger)cinemasCount {
    return _cinemasJSON.items.count;
}

- (SeanceJSONInfo *)cinemaAtIndex:(NSUInteger)index {
    SeanceJSONInfo *seanceInCinema = [_cinemasJSON.items objectAtIndex:index];
    return seanceInCinema;
}

@end
