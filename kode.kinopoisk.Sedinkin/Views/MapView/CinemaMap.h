//
//  CinemaMap.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import "SeanceJSONInfo.h"

@protocol CinemaMapViewDelegate <NSObject>

@required
- (void)closeCinemaMapView;
- (void)needToShowCinemaInfo:(NSString *)name address:(NSString *)address;

@end

@interface CinemaMap : UIView <GMSMapViewDelegate>

@property (weak, nonatomic) id<CinemaMapViewDelegate> cinemaMapDelegate;

- (void)setCameraToCinema:(SeanceJSONInfo *)cinema;

@end
