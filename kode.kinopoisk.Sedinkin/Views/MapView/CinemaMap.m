//
//  CinemaMap.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "CinemaMap.h"

@interface CinemaMap ()

@property (weak, nonatomic) IBOutlet GMSMapView *cinemaGoogleMap;

@end

@implementation CinemaMap

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setCameraToCinema:(SeanceJSONInfo *)cinema {
    
    _cinemaGoogleMap.delegate = self;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[cinema.lat floatValue]
                                                            longitude:[cinema.lon floatValue]
                                                                 zoom:13];
    [_cinemaGoogleMap setCamera:camera];
    _cinemaGoogleMap.myLocationEnabled = YES;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([cinema.lat floatValue], [cinema.lon floatValue]);
    marker.title = cinema.cinemaName;
    marker.snippet = cinema.address;
    marker.map = _cinemaGoogleMap;

}

- (void)drawRect:(CGRect)rect {
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 10);
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.6;
}

- (IBAction)closeMapView:(id)sender {
    [self.cinemaMapDelegate closeCinemaMapView];
}

#pragma mark - Google Map Delegate 

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    [_cinemaMapDelegate needToShowCinemaInfo:marker.title address:marker.snippet];
    return YES;
}

@end

