//
//  CinemaInfoView.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CinemaInfoView : UIView

@property (strong, nonatomic) NSString *cinema;
@property (strong, nonatomic) NSString *address;

@end
