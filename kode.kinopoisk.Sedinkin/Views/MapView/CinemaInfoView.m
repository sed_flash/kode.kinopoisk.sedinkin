//
//  CinemaInfoView.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 02.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "CinemaInfoView.h"

@interface CinemaInfoView ()

@property (weak, nonatomic) IBOutlet UILabel *cinemaNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cinemaAddressLabel;


@end

@implementation CinemaInfoView


- (void)drawRect:(CGRect)rect {
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 10);
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.6;
}

- (void)setCinema:(NSString *)cinema {
    [_cinemaNameLabel setText:cinema];
}

- (void)setAddress:(NSString *)address {
    [_cinemaAddressLabel setText:address];
}

@end
