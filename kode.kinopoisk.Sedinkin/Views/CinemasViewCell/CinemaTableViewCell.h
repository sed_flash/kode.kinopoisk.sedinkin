//
//  CinemaTableViewCell.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CinemaTableViewCell : UITableViewCell

@property (strong, nonatomic) NSString *cName;
@property (strong, nonatomic) NSString *cAddress;
@property (strong, nonatomic) NSString *sTime;

@end
