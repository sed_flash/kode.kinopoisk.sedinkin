//
//  CinemaTableViewCell.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "CinemaTableViewCell.h"
#import "Stylization.h"

@interface CinemaTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *cinemaName;
@property (weak, nonatomic) IBOutlet UILabel *cinemaAddress;
@property (weak, nonatomic) IBOutlet UILabel *seanceTime;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end

@implementation CinemaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.mainView.layer setCornerRadius:5.0];
    [self.mainView.layer setMasksToBounds:YES];
}

- (void)drawRect:(CGRect)rect {
    [[Stylization sharedInstance] drawGradientInView:_mainView
                                          startColor:RGBColor(0xf2f2f2, 1)
                                            endColor:RGBColor(0xdfdfdf, 1)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCName:(NSString *)cName {
    [_cinemaName setText:cName];
}

- (void)setCAddress:(NSString *)cAddress {
    [_cinemaAddress setText:cAddress];
}

- (void)setSTime:(NSString *)sTime {
    [_seanceTime setText:sTime];
}

@end
