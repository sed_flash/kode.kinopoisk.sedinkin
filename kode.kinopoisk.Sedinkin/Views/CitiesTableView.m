//
//  CitiesTableView.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "CitiesTableView.h"

#define CITY_CELL @"cityCell"

@interface CitiesTableView () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *cititesTable;
@end

@implementation CitiesTableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        frame.origin.x = 0;
        frame.origin.y = 0;
        _cititesTable = [[UITableView alloc] initWithFrame:frame];
        [_cititesTable registerClass:[UITableViewCell class] forCellReuseIdentifier:CITY_CELL];
        [_cititesTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_cititesTable setBackgroundColor:RGBColor(0xffd3b6, 1)];
        _cititesTable.delegate = self;
        _cititesTable.dataSource = self;
        [self addSubview:_cititesTable];
    }
    
    return self;
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.delegate citiesCount:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cityCell = [tableView dequeueReusableCellWithIdentifier:CITY_CELL];
    [cityCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    AdditionalCityInfo *cityInfo = [self.delegate cityForIndex:self index:indexPath.row];
    cityCell.textLabel.text = cityInfo.cityName;
    
    return cityCell;
}

#pragma makk - UITableView Delegate 

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [[cell textLabel] setFont:[UIFont fontWithName:@"Helvetica" size: 15.0]];
    [cell.textLabel setTextColor:RGBColor(0x151414, 1)];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AdditionalCityInfo *cityInfo = [self.delegate cityForIndex:self index:indexPath.row];
    [self.delegate setChoosingCity:cityInfo.cityName cityID:cityInfo.cityID];
    [self.delegate closeTableView];
}

@end
