//
//  FilmTableViewCell.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmTableViewCell : UITableViewCell

@property (copy, nonatomic) NSString *film;
@property (copy, nonatomic) NSString *genre;
@property (assign, nonatomic) float filmRating;

@end
