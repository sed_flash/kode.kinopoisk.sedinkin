//
//  FilmTableViewCell.m
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 01.10.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import "FilmTableViewCell.h"
#import "Stylization.h"

@interface FilmTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *filmName;
@property (weak, nonatomic) IBOutlet UILabel *filmGenre;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end

@implementation FilmTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.mainView.layer setCornerRadius:5.0];
    [self.mainView.layer setMasksToBounds:YES];
}

- (void)drawRect:(CGRect)rect {
    [[Stylization sharedInstance] drawGradientInView:_mainView
                                          startColor:RGBColor(0xf2f2f2, 1)
                                            endColor:RGBColor(0xdfdfdf, 1)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFilm:(NSString *)film {
    [_filmName setText:film];
}

- (void)setGenre:(NSString *)genre {
    [_filmGenre setText:genre];
}

- (void)setFilmRating:(float)filmRating {
    [_rating setText:[NSString stringWithFormat:@"%g", filmRating]];
}

@end
