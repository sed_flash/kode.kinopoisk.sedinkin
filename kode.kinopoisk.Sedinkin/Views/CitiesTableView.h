//
//  CitiesTableView.h
//  kode.kinopoisk.Sedinkin
//
//  Created by Vlad on 30.09.16.
//  Copyright © 2016 Vlad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdditionalCityInfo.h"
@class CitiesTableView;

@protocol CityTableViewDelegate <NSObject>

@required
- (NSUInteger)citiesCount:(CitiesTableView *)citiesTableView;
- (AdditionalCityInfo *)cityForIndex:(CitiesTableView *)citiesTableView index:(NSInteger)index;

@optional
- (void)setChoosingCity:(NSString *)city cityID:(int)cityID;
- (void)closeTableView;

@end

@interface CitiesTableView : UIView

@property (nonatomic, weak) id<CityTableViewDelegate> delegate;

@end
